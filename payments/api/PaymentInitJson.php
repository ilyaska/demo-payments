<?php
namespace payments\api;
use MrApp;
use MrConn;
use MrEnv;
use MrVar;
use mrcms\actions\ActionParamsJson;
use payments\paymentsystems\PaymentSystem;

require_once 'mrcms/actions/ActionParamsJson.php';
require_once 'payments/paymentsystems/PaymentSystem.php';

/**
 * Проверка и Создание формы для отправки заявки на пополнение депозита.
 *
 * @author     Ilyas E. Kalimullin <the.ilyas@gmail.com>
 * @package    payments
 * @subpackage api
 */
class PaymentInitJson extends ActionParamsJson
{

    ################################### Properties ###################################

    /**
     * Массив ожидаемых параметров из вне.
     *
     * @var    array
     */
    /*__override__*/ protected $_params = array
    (
        'userAccount'         => array('isRequire' => true, 'maxLength' => 10, 'type' => MrVar::T_INT,
                                        'validators' => array('number' => array('source' => '\mrcore\validators\Number'))),
        'amount'              => array('isRequire' => true, 'maxLength' => 8, 'type' => MrVar::T_FLOAT,
                                        'validators' => array('number' => array('source' => '\mrcore\validators\Number',
                                        'attrs' => array('isFloat' => true, 'minValue' => 1, 'maxValue' => 300000)))), // max limit in rubles
        'cardCurrency'        => array('isRequire' => true),
        'customerAgreement'   => array('type' => MrVar::T_BOOL, 'isRequire' => true, 'skip' => true),
    );

    /**
     * Массив допустимых значений валют пополнения
     * @var array
     */
    protected $_cardCurrencies = array(
        'PastaMoney' => array(
            'USD' => 'USD',
            //'EUR' => 'EUR',
        ),

        'YandexKassa' => array(
            'RUB' => 'RUB',
        ),

        'Moneta' => array(
            'USD' => 'USD',
        ),

        'PaymentCenter' => array(
            'USD' => 'USD',
        ),

        'Skrill' => array(
            'USD' => 'USD',
        ),
    );

    #################################### Methods #####################################

    /**
     * Дополнительная валидация параметров, когда уже все
     * параметры инициализированы, проверены стандартными валидаторами,
     * а также явно приведены к соответствующим типам
     * (если в настройках параметра указан тип).
     *
     * @author     Ilyas E. Kalimullin <the.ilyas@gmail.com>
     */
    /*__override__*/ protected function _validate()
    {
        $conn = &MrConn::db();
        //var_dump($this->_params);
        if ($this->_params['userAccount']['isValid'])
        {
            $result = $conn->fetchRow(
                "SELECT account as id, is_deposit, service_id 
                 FROM accounts 
                 WHERE account = ? ", array($this->_params['userAccount']['value'])
            );

            if (empty($result['id']))
            {
                $this->_errors[] = array('userAccount', __tmp('Account not found'));
            }
            else if (0 && $result['is_deposit'] == 0) //Оля сказала убрать все ненужные проверки
            {
                $this->_errors[] = array('userAccount', __tmp('Deposit not available'));
            }
            else {
                $account_info = $conn->fetchRow(
                    "SELECT c.currency_name AS currency FROM services s
                        LEFT join currencies c ON (s.currency_id = c.currency_id) 
                        WHERE service_id = ?", array($result['service_id'])
                );
                
                if (empty($account_info['currency']) ) // || $account_info['currency'] != 'EUR'
                {
                    $this->_errors[] = array('userAccount', __tmp('Deposit not available for this type of account currency'));
                }
            }

            if (!isset($this->_cardCurrencies[$this->_context['payment-name']][$this->_params['cardCurrency']['value']]))
            {
                $this->_errors[] = array('cardCurrency', __tmp('Wrong card currency'));
            }

            if ($this->_params['cardCurrency']['value'] == 'USD' && $this->_params['amount']['value'] > 10000)
            {
                $this->_errors[] = array('amount', __tmp('Amount must be less than 10000'));
            }
            elseif ($this->_params['cardCurrency']['value'] == 'RUB' && $this->_params['amount']['value'] > 300000)
            {
                $this->_errors[] = array('amount', __tmp('Amount must be less than 300000'));
            }
            elseif ($this->_params['cardCurrency']['value'] == 'EUR' && $this->_params['amount']['value'] > 10000)
            {
                $this->_errors[] = array('amount', __tmp('Amount must be less than 300000'));
            }

        }

    }

    /**
     * Формируется массив данных, которые затем преобразуются в json формат
     * и отправляется клиенту в виде ответа.
     *
     * @author     Ilyas E. Kalimullin <the.ilyas@gmail.com>
     * @param      array  $response
     * @throws     \Exception
     */
    /*__override__*/ protected function _createResponse(array &$response)
    {
        $paymentSystem = &PaymentSystem::factory($this->_context['payment-source'], array('POST' => $_POST, 'account'=> $this->_params['userAccount']['value']));

        $result = $paymentSystem->initPaymentForm($this->_params['amount']['value'], $this->_params['cardCurrency']['value']);

        if (!$paymentSystem->isError())
        {
            $response['result'] = $result;
        }
        else
        {
            $this->_errors[] = array('userAccount', __tmp($paymentSystem->getAllErrors()));
            //var_dump($this->errors);
        }
    }

}
