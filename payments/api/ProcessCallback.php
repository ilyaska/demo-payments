<?php
namespace payments\api;
use MrApp;
use MrVar;
use mrcore\lib\Format;
use mrcms\actions\ActionCallback;
use currencies\models\Currency;
use payments\paymentsystems\PaymentSystem;

require_once 'mrcore/MrSettings.php';
require_once 'mrcore/lib/Format.php';
require_once 'mrcms/actions/ActionCallback.php';
require_once 'currencies/models/Currency.php';
require_once 'payments/paymentsystems/PaymentSystem.php';

/**
 * Общий колбек от платежек.
 *
 * @author     Ilyas E. Kalimullin <the.ilyas@gmail.com>
 * @package    payments
 * @subpackage api
 */
class ProcessCallback extends ActionCallback
{

    ################################### Properties ###################################

    /**
     * Название шаблона уведомления.
     * (см. таблицу site_notifications_templates)
     *
     * @var    string
     */
    /*__override__*/ protected $_noticeName = 'callback.payment.deposit.notice';

    #################################### Methods #####################################

    /**
     * Формируется массив данных, которые затем преобразуются в json формат
     * и отправляется клиенту в виде ответа.
     *
     * @author     Ilyas E. Kalimullin <the.ilyas@gmail.com>
     * @param      array  $data (copy of $_POST array)
     * @param      array  $response
     * @throws     \Exception
     */
    /*__override__*/ protected function _processing(array $data, array &$response)
    {
        $isServer = MrVar::getInt('server');

        $paymentSystem = &PaymentSystem::factory($this->_context['payment-source'], array('POST' => $data, 'serverCall' => $isServer));
        $paymentSystem->bindEvent('log', function (array $args) { $this->_log($args[0]); });
        $paymentSystem->bindEvent('sendNotice', function (array $args) { $this->_sendNotice($args[0]); });

        if ($isServer)
        {
            $response['code'] = $paymentSystem->getCbHttpCode();
        }

        $paymentSystem->processTransaction();

        if ($isServer)
        {
            $response['text'] = $paymentSystem->getResultMsg();
        }
        else
        {
            // header('Location: /payments/'. $paymentSystem->getResultUrl() . '/' . ($paymentSystem->isError() ? '/error/824/': '/success/'));
            $url = '/payments/'. $paymentSystem->getResultUrl();
            MrApp::$rsp->setRedirect(\mrcore\base\BuilderLink::factory($url), 302);
        }
    }

    /**
     * Отправка уведомления.
     *
     * @author     Ilyas E. Kalimullin <the.ilyas@gmail.com>
     * @param      array     $data
     * @return     boolean
     */
    /*__override__*/ protected function _sendNotice(array $data)
    {
        if (Currency::convert($data['amount'], $data['currency'], 'EUR') > $data['notifyLimit']) // лимит достается из настроек
        {
            $data['paymentName'] = $this->_context['payment-name'];
            $data['amount'] = Format::money($data['amount'], 2);
            $data['transactionAmount'] = Format::money($data['transactionAmount'], 2);

            return parent::_sendNotice($data);
        }

        return true;
    }

}