<?php
namespace payments\paymentsystems;
use Exception;
use MrConn;
use MrSettings;
use mrcore\base\TEvents;
use urrencies\models\Currency;

require_once 'mrcore/MrSettings.php';
require_once 'currencies/models/Currency.php';
require_once 'mrcore/base/TEvents.php';

/**
 * Абстрактный класс для систем пополнения депозита
 *
 * @author     Ilyas E. Kalimullin <the.ilyas@gmail.com>
 * @package    lh.payments
 * @subpackage api
 */
abstract class PaymentSystem
{
    use TEvents;

    ################################### Properties ###################################

    protected $_name = ''; // псевдоним, например qiwi, yandexmoney

    private $tpMt5 = null; // коннект к mt5
    private $dbConn = null; // коннект к mysql
    protected $_responseStatus = '';
    protected $_responseUrl = ''; // return url for user redirect

    /**
     *  Показывает был ли вызов совершен сервером или пользователем
     */
    protected $_isServerCall = 0;

    /**
     *  Показывает возникли ли ошибки на этапе выполнения
     */
    protected $_isError = 0;
    protected $_errors = array();

    protected $_postData = '';

    /**
     * id аккаунта
     * @var int|mixed
     */
    protected $_account = 0;

    /**
     * Свойства аккаунта
     * @var array
     */
    protected $_accountInfo = array();

    /**
     * Сумма пополнения
     * @var float?
     */
    protected $_paidAmount = 0;

    /**
     * Валюта пополнения
     * @var string
     */
    protected $_paidCurrency = '';
    
    /**
     * Требуемый код ответа сервера во время колбека
     *
     * @var    int
     */
    protected $_cbHttpCode = 200;

    /**
     * Текст ответа сервера серверу в случае успеха
     */
    protected $_returnMsgOk = '';

    /**
     * Текст ответа сервера серверу в случае ошибки
     */
    protected $_returnMsgFail = '';

    /**
     * @var array Настройки платежной системы
     */
    private $_paySettings = array();

    #################################### Methods #####################################

    /**
     * Фабрика
     *
     * @author     Ilyas E. Kalimullin <the.ilyas@gmail.com>
     * @param      string $source
     * @param      array $params
     * @return     PaymentSystem
     * @throws     Exception
     */
    public static function &factory($source, array $params)
    {
        /*__assert__*/ assert('is_string($source); // VALUE is not a string');

        // попытка подключения класса
        include_once strtr(ltrim($source, '\\'), '\\', '/') . '.php';

        $result = new $source($params);

        if (!($result instanceof self))
        {
            throw new Exception(sprintf('Класс %s не является наследником класса PaymentSystem', $source));
        }

        return $result;
    }

    /**
     * Конструктор класса.
     *
     * @author     Ilyas E. Kalimullin <the.ilyas@gmail.com>
     * @params array
     */
    public function __construct($params)
    {
        $this->_postData = $params['POST'];
        $this->_isServerCall = isset($params['serverCall']) ? $params['serverCall'] : 0;
        $this->_account = isset($params['account']) ? $params['account'] : 0;
    }

    /*
     *  Возвращает свойство _responseUrl
     */
    public function getResponseUrl()
    {
        return $this->_responseUrl;
    }

    /**
     * Возвращает урл платежки (для конечного редиректа)
     * @return mixed
     */
    public function getResultUrl()
    {
        return $this->_url . '/' . ($this->isError() ? '/error/824/' : '/success/');
    }

    public function isError()
    {
        return $this->_isError;
    }

    /**
     * Возвращает текст ответа для сервера в зависимости от ошибки или хорошего результата
     * @return mixed
     */
    public function getResultMsg()
    {
        return $this->isError() ? $this->_returnMsgFail : $this->_returnMsgOk ;
    }

    /**
     * Возвращает все найденные ошибки через запятую
     *
     * @return string
     */
    public function getAllErrors()
    {
        return implode(',', $this->_errors);
    }

    /**
     * Возвращает код серверного ответа
     *
     * @return string
     */
    public function getCbHttpCode()
    {
        return $this->_cbHttpCode;
    }

    /**
     * Абстрактный метод инициализации формы оплаты (реализуется в классе платежки)
     *
     * @param $amount
     * @param $currency
     * @return mixed
     */
    abstract public function initPaymentForm($amount, $currency);

    protected function getParam($param)
    {
        return isset($this->_postData[$param]) ? (string)$this->_postData[$param] : '';
    }

    /**
     * Выполнение транзакци у нас (с разбиением на этапы)
     *
     * @param $id
     * @param $extId
     */
    protected function runTransaction($id, $extId)
    {
        $this->initPaymentSettings($this->_accountInfo['currencyName'], $this->_accountInfo['jurisdiction']);

        $this->acceptTransaction($id);

        $this->makeDeposit($this->_account, $this->_accountInfo['currencyName']);

        $this->_log("Send mail notify transaction: $id");
        $this->_sendNotice($id);

    }

    /**
     * Получаем уникальный номер транзакции
     *
     * @return int
     */
    protected function getUniqTransactionId()
    {
        return MrConn::getSequenceId('paymentTrans');
    }

    /**
     * Инициализирует свойства аккаунта и проверяем, что совпадает юрисдикция с платежкой
     *
     * @param $account
     */
    protected function checkAccount($account)
    {
        $this->getAccountInfo($account);

        // Проверяем, что платежка включена
        $res = MrConn::db()->fetchRow(
            "SELECT method_jurisdiction 
                  FROM payment_systems 
                  WHERE var_name = ? AND (method_status = 'enabled' or method_status = 'hidden') AND method_type='deposit' AND method_jurisdiction = ?",
            array($this->_name, $this->_accountInfo['jurisdiction']));

        if (empty($res['method_jurisdiction'])) 
        {
            $this->_isError = 1;
            $this->_errors[] = 'Payment system disabled';
        }
    }

    /**
     * Ставит флаг о зачислении транзакции на счет, должно предотвратить повторное зачисление
     * @param $id
     */
    protected function acceptTransaction($id)
    {
        MrConn::db('tpdata')->execQuery(
            "UPDATE payment_deposit SET is_deposit = 1 WHERE transaction_hash = ?",
            array($id));
    }

    /**
     * Обновляем свойства существующей транзакции
     *
     * @param $id
     * @param $extId
     * @param $status
     */
    protected function updateTransaction($id, $extId, $status)
    {
        MrConn::db('tpdata')->execQuery(
            "UPDATE payment_deposit 
                 SET status = ?, updated = NOW(), transaction_provider = ? 
                 WHERE transaction_hash = ?",
            array($status, $extId, $id));
    }

    /**
     * Получаем свойства предварительно сохраненной транзакции
     * @param $id
     * @return array
     */
    protected function getTransaction($id)
    {
        return MrConn::db('tpdata')->fetchRow(
            "SELECT account, created, amount, provider, currency, is_deposit, updated  
                  FROM payment_deposit WHERE transaction_hash = ?", array($id));
    }

    /**
     * Сохраняем созданную транзакцию у нас в бд
     *
     * @param $trHash
     * @param $account
     * @param $amount
     * @param $cardCurrency
     */
    protected function storeTransaction($trHash,$account,$amount,$cardCurrency)
    {
        $result = MrConn::db('tpdata')->execQuery("INSERT INTO 
                                                        payment_deposit (transaction_hash, account, created, amount, provider, currency) 
                                                        VALUES (? ,? ,now() ,? , ? , ?)",
            array($trHash, $account, $amount, $this->_name, $cardCurrency));
    }

    /**
     * Зачисление денег на счет, состоит из нескольких записей  (согласно ТЗ)
     *
     * @param $account номер счета
     * @param $accountCurrency
     * @param $amount
     * @param $currency
     */
    protected function makeDeposit($account, $accountCurrency)
    {
        $amount = $this->_paidAmount;
        $currency = $this->_paidCurrency;
        $newamount = 0; // сумма к зачислению в валюте счета

        $this->tpMt5 = MrConn::get('tp', $this->_mt5Server);

        $this->_log("MT5 trying to deposit account $account amount:$amount currency:$currency");
        $this->depositAccount(array($account, 0, 'Deposit ' . $amount . ' ' . $currency));

        if ($accountCurrency != $currency)
        {
            $this->_log("Conversion to $accountCurrency");
            $this->depositAccount(array($account, 0, 'Client asks conversation to ' . $accountCurrency));


            $rate = $this->getConversionRate($currency, $accountCurrency);

            $this->_log("Conversion rate $rate");
            $this->depositAccount(array($account, 0, 'conversion rate = ' . $rate));
            $newamount = Currency::convert($amount, $currency, $accountCurrency);
        }
        else
        {
            $newamount = $amount;
        }

        $this->_log(sprintf("Trying to deposit account %s amount: %s comment:%s", $account, $newamount, $this->getPayParam('depositComment')));
        $this->depositAccount(array($account, $newamount, $this->getPayParam('depositComment')));  //'Deposit ...'

        // дублирование сделок на запасной акк, согласно тз дублируем только одну сделку общую
        $this->backupTransaction($account, $amount, $currency, $newamount, $accountCurrency);

    }

    /**
     * Зачисление средств на баланс
     *
     * @param $params
     */
    protected function depositAccount($params)
    {
        $account = (int)$params[0];
        $amount = (float)$params[1];
        $comment = (string)$params[2];
        $type = isset($params[3]) ? (int)$params[3] : 2;

        $this->tpMt5->depositAccount($account, $amount, $comment, $type);
    }

    /**
     * Функция возвращает курс конвертации одной валюты в другую
     *
     * @param $from  валюта источник
     * @param $to    валюта куда хотим конвертировать
     * @return float
     */
    protected function getConversionRate($from, $to)
    {
        $rate = Currency::getQuote($from . $to);

        if (!$rate && ($from == 'EUR' || $from == 'USD'))
        {
            // try to invert
            $rate = Currency::getQuote($to . $from);

            if ($rate)
            {
                $rate = round(1 / $rate, 6);
            }
        }

        if (!$rate)
        {
            trigger_error("No currency rate from $from to $to", E_USER_NOTICE);
        }

        return $rate;
    }

    /**
     * Дублирование сделки на запасной акк
     *
     * @param $account
     * @param $amount
     * @param $currency
     * @param $newamount
     * @param $accountCurrency
     */
    protected function backupTransaction($account, $amount, $currency, $newamount, $accountCurrency)
    {
        if (!($this->getPayParam('backupAccount') > 0))
        {
            return;
        }

        $comment = "'$account': $amount{$currency} ($newamount{$accountCurrency})";
        $this->depositAccount(array((int)$this->getPayParam('backupAccount'), $newamount, $comment));
    }

    /**
     * Получаем параметр настройки, либо значение по умолчанию
     *
     * @param $name
     * @return int|mixed|string
     */
    protected function getPayParam($name)
    {
        $default = '';

        if ($name == 'commission' || $name == 'compensationBonus')
        {
            $default = 10; // на всякий случай по умолчанию коммисия 10%
        }

        return isset($this->_paySettings[$name]) ? $this->_paySettings[$name] : $default;
    }

    /**
     * Реализуем 4 уровня настроек на базе MrSettings
     * начиная от самой общей
     *
     * @param $jurisdiction - юрисдикция аккаунта
     * @param $currency
     */
    protected function initPaymentSettings($accountCurrency, $jurisdiction)
    {
        $this->getPaymentSettings('*','*');
        $this->getPaymentSettings('*', $this->getCurrencyType($accountCurrency));
        $this->getPaymentSettings('*', $accountCurrency);
        $this->getPaymentSettings($jurisdiction, '*');
        $this->getPaymentSettings($jurisdiction, $this->getCurrencyType($accountCurrency));
        $this->getPaymentSettings($jurisdiction, $accountCurrency);
    }

    /**
     * Получаем настройки по заданной юрисдикции и валюте
     *
     * @param $jurisdiction
     * @param $currency
     */
    protected function getPaymentSettings($jurisdiction, $currency)
    {
        $group = $jurisdiction . ':payments-' . $this->_name . ':' . $currency;

        // присоединяем результаты, перезаписывая старые, то есть более поздний уровень - главный
        $this->_paySettings = array_merge($this->_paySettings, MrSettings::getGroup($group));
    }

    /**
     * Возвращает тип валюты fiat|crypto
     *
     * @param $currency
     * @return string
     */
    protected function getCurrencyType($currency)
    {
        if (in_array($currency, array('USD', 'EUR')))
        {
            return 'fiat';
        }

        return 'crypto';
    }

    /**
     * Достаем свойства аккаунта из бд
     *
     * @param $account
     */
    protected function getAccountInfo($account)
    {
        if (isset($this->_accountInfo['id']))
        {
            return;
        }

        $result = MrConn::db()->fetchRow(
            "SELECT lt.account as id, lt.service_id as service_id, lt.fr_number, 
                        lt.date_of_registration, lt.city_name, lt.user_phone, lt.adviser, 
                        CONCAT_WS(' ', ssu.user_name, ssu.user_surname) as user_fio,
                        ssu.user_email as user_email, lt.user_comments as user_comment,
                        lt.manager as user_manager,
                        s.service_jurisdiction as jurisdiction,
                        c.currency_name as currencyName  
                     FROM
                     accounts lt 
                     JOIN
                     security_users ssu
                     ON
                     lt.user_id = ssu.user_id
                     LEFT JOIN
                     services s 
                     ON 
                     lt.service_id = s.service_id
                     LEFT JOIN 
                     currencies c ON (s.currency_id = c.currency_id)
                    WHERE lt.account = ? ", array($account)
        );

        if (empty($result['id']))
        {
            $this->_isError = 1;
            $this->_errors[] = "Account $account not found";
            return;
        }

        $this->_accountInfo = $result;
        $this->_account = $result['id'];
    }

    /**
     * Получение детальных данных транзакции.
     *
     * @param       string  $transactionId
     * @return      bool|array
     */
    private function _getTransactionDetails($transactionId)
    {
        /*__assert__*/ assert('is_string($transactionId); // VALUE is not a string');

        $transaction = $this->getTransaction($transactionId);

        if (!isset($transaction['account']))
        {
            return false;
        }

        $result = array();

        $this->getAccountInfo($transaction['account']);

        $accountInfo = $this->_accountInfo;
        $result['userAccount'] = (int)$transaction['account'];

        // сумма в валюте транзакции
        $result['transactionAmount'] = (float)$transaction['amount'];
        $result['transactionCurrency'] = $transaction['currency'];

        // в валюте счета
        $result['amount'] = Currency::convert((float)$transaction['amount'], $transaction['currency'], $accountInfo['currencyName']);
        $result['currency'] = $accountInfo['currencyName'];

        $result['datetimeCreated'] = $transaction['updated'];
        $result['notifyLimit'] = $this->getPayParam('notifyLimit');

        return $result;
    }

    /**
     * Отправка уведомнения о транзакции
     *
     * @param      string  $transactionId
     */
    final protected function _sendNotice($transactionId)
    {
        /*__assert__*/ assert('is_string($transactionId); // VALUE is not a string');

        if (false !== ($data = $this->_getTransactionDetails($transactionId)))
        {
            $this->invokeEvent('sendNotice', array($data));
        }
    }

    /**
     * Логирование указанного сообщения
     *
     * @param      string  $message
     */
    final protected function _log($message)
    {
        /*__assert__*/ assert('is_string($message); // VALUE is not a string');

        $this->invokeEvent('log', array($message));
    }

}