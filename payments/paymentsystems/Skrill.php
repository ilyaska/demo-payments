<?php
namespace payments\paymentsystems;

require_once 'mrcore/MrSettings.php';
require_once 'payments/paymentsystems/PaymentSystem.php';
require_once 'payments/config/skrill/' . (MRCORE_SERVER_NAME == 'host' ? 'real' : 'demo') . '/config.php';
require_once 'currencies/models/Currency.php';


/**
 * Класс Skrill
 * Документация https://www.skrill.com/fileadmin/content/pdf/Skrill_Quick_Checkout_Guide.pdf
 *
 * Инициализируем платежную форму и получаем урл для редиректа пользователя
 * Далее ожидаем коллбек
 *
 * @author     Ilyas E. Kalimullin <the.ilyas@gmail.com>
 * @package    payments
 * @subpackage api
 */
class Skrill extends PaymentSystem
{

    ################################### Properties ###################################

    protected $_name = 'skrill'; // Short name
    protected $_url = 'skrilldone'; // урл при пополнении и возврат результата

    protected $_cbHttpCode = 200;

    protected $_returnMsgOk = 'SUCCESS';
    protected $_returnMsgFail = 'FAIL';

    #################################### Methods #####################################

    public function __construct($params)
    {
        parent::__construct($params);
    }

    public function processTransaction()
    {
        if (empty($this->_postData))
        {
            $this->_log('Empty request');
            $this->_isError = 1;
            return;
        }

        $trId = $this->getParam('MNT_TRANSACTION_ID'); // our transaction number
        $prId = $this->getParam('MNT_OPERATION_ID'); // their transaction number

        $this->_log("Post command received. Transaction: $trId/$prId" .
            ", Amount=" . $this->getParam('MNT_AMOUNT') .
            ", Currency=" . $this->getParam('MNT_CURRENCY_CODE') .
            ", Subscriber=" . $this->getParam('MNT_SUBSCRIBER_ID') .
            ", Test=" . $this->getParam('MNT_TEST_MODE') .
            ", MNT_ID=" . $this->getParam('MNT_ID') .
            ", CUSTOM1=" . $this->getParam('MNT_CUSTOM1')
        );

        // Form and check the signature
        $_testSignature = md5($this->getParam('MNT_ID') .
            $this->getParam('MNT_TRANSACTION_ID') .
            $this->getParam('MNT_OPERATION_ID') .
            $this->getParam('MNT_AMOUNT') .
            $this->getParam('MNT_CURRENCY_CODE') .
            $this->getParam('MNT_SUBSCRIBER_ID') .
            $this->getParam('MNT_TEST_MODE') .
            SKRILL_SECRET_KEY
        );

        if ($_testSignature != $this->getParam('MNT_SIGNATURE'))
        {
            $this->_log(sprintf("Signature mismatch: our=%s, their=%s", $_testSignature, $this->getParam('MNT_SIGNATURE')));
            $this->_isError = 1;
            return;
        }

        // Get transaction properties
        $transaction = $this->getTransaction($trId);
        if (empty($transaction['account']))
        {
            $this->_log(sprintf("Transaction: %s/%s not found", $trId, $prId));
            $this->_isError = 1;
            return;
        }

        // Updating transaction status
        $this->updateTransaction($trId, $prId, 'success'); // В этой платежке колбек приходит только при успешном пополнении

        // Checking account
        $this->getAccountInfo($transaction['account']);
        if (empty($this->_accountInfo['id']))
        {
            $this->_log(sprintf("Transaction: %s/%s. Account %s not found", $trId, $prId, $transaction['account']));

            $this->_isError = 1;
            return;
        }

        // Is new transaction?
        if ($transaction['is_deposit'] == 0)
        {
            $this->_paidAmount = (float)$this->getParam('MNT_AMOUNT');
            $this->_paidCurrency = (string)$this->getParam('MNT_CURRENCY_CODE');

            $this->runTransaction($trId, $prId);
        }
    }

    public function initPaymentForm($amount, $cardCurrency)
    {
        // Checking account
        $this->checkAccount($this->_account);

        if ($this->_isError) return; // запрет платежки в родительском классе

        $trHash = $this->getUniqTransactionId();
        if (!$trHash) $this->_isError = 1;

        $this->storeTransaction($trHash, $this->_account, $amount, $cardCurrency);

        $result = array();
        $result['input']['merchant_id'] = SKRILL_SHOP_ID;
        $result['input']['transaction_id'] = $trHash;
        $result['input']['amount'] = sprintf("%0.2f", $amount); // Намеренно используем обрезание до двух знаков
        $result['input']['currency'] = $cardCurrency;
        $result['input']['merchant_fields'] = 'account'; // через запятую можно перечислить доп поля, потом они вернутся в коллбеке
        $result['input']['account'] = $this->_account;
        $result['input']['return_url'] = SKRILL_USERRETURN_URL;
        $result['input']['status_url'] = SKRILL_CALLBACK_URL . '?server=1';

        $result['url'] = SKRILL_URL . '?sid=' .$this->getSid(SKRILL_URL, $result['input']);
        $result['method'] = 'GET'; 
        $result['input'] = array(); // зануляем

        return $result;
    }

    /*
     *  Передаем параметры и получаем айди сессии от скрила
     */
    private function getSid($url, $data)
    {
        $data['prepare_only'] = 1;
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_TIMEOUT, 7);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));

        $result = curl_exec($ch); // Call 
        $curl_error = curl_error($ch); // Collect errors

        if ($curl_error)
        {
            $this->_isError = 1;
        }
        
        $curl_info = curl_getinfo($ch);
        curl_close($ch);

        return $result;
    }
}